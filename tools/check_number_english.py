import enchant
from pyvi import ViTokenizer, ViPosTagger
d = enchant.Dict("en_US")
def is_number(s):
    count = 0
    try:
        float(s)
        return True
    except ValueError:
        count += 1
    try:
        float(s.replace(',', '.'))
        return True
    except ValueError:
        count += 1
    if count == 2:
        return False
    return False
def have_number(word):
    ls_c = list(word)
    for c in ls_c:
        if c in '0123456789':
            return True
    return False
def check_english(word):
    return d.check(word)
def check_number_english(word):
    if is_number(word) == True or check_english(word)==True:
        return True
    return False

def have_special_char(word):
    ls_c = list(word)
    count_c = 0
    for c in ls_c:
        if c in 'abcdefghijklmnopqrstuvwxyzáàảãạăắằẳẵặâấầẩẫậéèẻẽẹêếềểễệíìỉĩịóòỏõọôốồổỗộơớờởỡợúùủũụưứừửữựýỳỷỹỵđ':
            count_c += 1
    if count_c==len(ls_c):
        return False
    return True

def check_have_number_english(word):
    if have_number(word) == True or check_english(word)==True:
        return True
    return False

def check_have_number_english_special_char(word):
    if have_number(word) == True or check_english(word)==True or have_special_char(word)==True:
        return True
    return False

def check_abbreviation(text, word):
    ls_abb = []
    w, n = ViPosTagger.postagging(ViTokenizer.tokenize(text))
    for i, v in enumerate(w):
        if n[i]=='Ny' or n[i]=='Np':
            ls_abb.append(v.lower())
    if word in ls_abb:
        return True
    return False