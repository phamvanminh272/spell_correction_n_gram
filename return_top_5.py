from nltk.tokenize import word_tokenize
import re
from lm.lm import KenLM
import editdistance
from tools.check_number_english import check_have_number_english_special_char, check_abbreviation

with open('./checkpoint/new_dictionary.txt') as f:
    dict = f.read()

dict_ls = [i for i in word_tokenize(dict)]
def preprocessing(text):
    text = text.lower()
    text = text.replace('\n', '')
    text = text.strip()
    return text
def check_real_word(word):
    if word in dict:
        return True
    else:
        return False

def generate_confuse_words(word):
    ls_w = []
    if len(word) >2 and 'ch' in word[:2]:
        ls_w.append(word)
        ls_temp = [i for i in word]
        ls_temp[0] = 't'
        ls_temp[1] = 'r'
        ls_w.append(''.join(ls_temp))
    if len(word) >2 and 'tr' in word[:2]:
        ls_w.append(word)
        ls_temp = [i for i in word]
        ls_temp[0] = 'c'
        ls_temp[1] = 'h'
        ls_w.append(''.join(ls_temp))
    if 's' in word[0]:
        ls_w.append(word)
        ls_temp = [i for i in word]
        ls_temp[0] = 'x'
        ls_w.append(''.join(ls_temp))
    if 'x' in word[0]:
        ls_w.append(word)
        ls_temp = [i for i in word]
        ls_temp[0] = 's'
        ls_w.append(''.join(ls_temp))
    if 'd' in word[0]:
        ls_w.append(word)
        ls_temp = [i for i in word]
        ls_temp[0] = 'r'
        ls_w.append(''.join(ls_temp))
        ls_temp[0] = 'gi'
        ls_w.append(''.join(ls_temp))
    if 'r' in word[0]:
        ls_w.append(word)
        ls_temp = [i for i in word]
        ls_temp[0] = 'd'
        ls_w.append(''.join(ls_temp))
    if len(word) >2 and 'gi' in word[:2]:
        ls_w.append(word)
        ls_temp = [i for i in word]
        ls_temp[0] = 'd'
        ls_temp[1] = ''
        ls_w.append(''.join(ls_temp))
    if 'c' in word[-1:]:
        ls_w.append(word)
        ls_temp = [i for i in word]
        ls_temp[-1] = 't'
        ls_w.append(''.join(ls_temp))
    if 't' in word[-1:]:
        ls_w.append(word)
        ls_temp = [i for i in word]
        ls_temp[-1] = 'c'
        ls_w.append(''.join(ls_temp))
    if 'n' in word[-1:]:
        ls_w.append(word)
        ls_temp = [i for i in word]
        ls_temp[-1] = 'ng'
        ls_w.append(''.join(ls_temp))
    if 'ng' in word[-1:]:
        ls_w.append(word)
        ls_temp = [i for i in word]
        ls_temp[-1] = 'n'
        ls_temp[-2] = ''
        ls_w.append(''.join(ls_temp))
    if 'i' in word:
        ls_w.append(word)
        ls_w.append(word.replace('i', 'y'))
    if 'y' in word:
        ls_w.append(word)
        ls_w.append(word.replace('y', 'i'))
    ls = [i for i in ls_w if check_real_word(i)]
    if len(ls)==1:
        ls = []
    # print(ls)
    return ls

def parse_result(string):
    ls_temp = string.split(';')
    ls_rs = []
    for i in ls_temp:
        print(i)
        ls_ws = i.split()
        if len(ls_ws)<=1:
            continue
        print(ls_ws)
        ls_rs.append({'error_word': ls_ws[0], 'index': ls_ws[1], 'predicted_words': ls_ws[2:], 'first_word': ls_ws[2]})
    print(ls_rs)
    return ls_rs


def similarities_by_edit_distance(word, distance=2):
    """
    generate a list of candidates which have edit distance <= 2 with the non-word error
    :param word: non-word error
    :param distance: edit distance {1, 2, 3, 4, ...}
    :return: a list of candidates
    """
    ls_temp = []
    for i in dict_ls:
        if editdistance.eval(i, word) <= distance:
            ls_temp.append(i)
    # print(ls_temp)
    return ls_temp
class Predict:
    def __init__(self, wlm_path):
        print('Loading language model ...')
        self.wlm = KenLM(wlm_path)
    def beam_lm(self, text):
        sentence = preprocessing(text)
        words = word_tokenize(sentence)
        for index, word in enumerate(words):
            if not check_real_word(word) and not check_have_number_english_special_char(word) and not check_abbreviation(text, word):
                print('Non-word error:', word)
                words[index] = '*'
        text_copy = ' '.join(words)
        rs = ''
        def beam_lm_(text_uncertain, rs):
            uncetainties = [m.span() for m in re.finditer('\\*+', text_uncertain)]
            if len(uncetainties)==0:
                return text_uncertain, rs
            ls_words_text = word_tokenize(text)
            ls_words_uncertain = word_tokenize(text_uncertain)
            for i, v in enumerate(ls_words_uncertain):
                if v!= '*':
                    continue
                error_word = ls_words_text[i]
                w_list = similarities_by_edit_distance(error_word)
                if len(w_list)==0:
                    return text_uncertain
                # left context
                left_context = ' '.join(ls_words_uncertain[0:i])
                if i == 0:
                    left_context = ''
                # right context, remain context
                remain_context = ''
                right_context = ''
                if i < len(ls_words_uncertain) - 1:
                    ls_words_right_context = []
                    for index, w in enumerate(ls_words_uncertain[i + 1:]):
                        if w == '*':
                            if index < len(ls_words_uncertain) - 1:
                                remain_context = ' '.join(ls_words_uncertain[index + i + 1:])
                            break
                        ls_words_right_context.append(w)
                    right_context = ' '.join(ls_words_right_context)

                candidates = []
                for wo in w_list:
                    candidate = left_context + ' ' + wo + ' ' + right_context
                    score = self.wlm.score(candidate)
                    candidates.append({'candidate': candidate, 'word': wo, 'score': score})
                candidates = sorted(candidates, key=lambda i: i['score'], reverse=True)
                rs = rs + '; ' + error_word + ' ' + str(i) + ' ' + ' '.join([i['word'] for i in candidates[:5]])
                best_candidate = candidates[0]['candidate'] + ' ' + remain_context
                return beam_lm_(best_candidate, rs)
        return beam_lm_(text_copy, rs)
    def beam_lm2(self, text):
        sentence = preprocessing(text)
        words = word_tokenize(sentence)
        for index, word in enumerate(words):
            if check_real_word(word) and not check_have_number_english_special_char(word) and not check_abbreviation(text, word):
                if len(generate_confuse_words(word))>0:
                    words[index] = '*'
        text_copy = ' '.join(words)
        rs = ''
        def beam_lm_(text_uncertain, rs):
            uncetainties = [m.span() for m in re.finditer('\\*+', text_uncertain)]
            if len(uncetainties)==0:
                return text_uncertain, rs
            ls_words_text = word_tokenize(text)
            ls_words_uncertain = word_tokenize(text_uncertain)
            for i, v in enumerate(ls_words_uncertain):
                if v!= '*':
                    continue
                error_word = ls_words_text[i]
                w_list = generate_confuse_words(error_word)
                if len(w_list)==0:
                    return text_uncertain
                # left context
                left_context = ' '.join(ls_words_uncertain[0:i])
                if i == 0:
                    left_context = ''
                # right context, remain context
                remain_context = ''
                right_context = ''
                if i < len(ls_words_uncertain) - 1:
                    ls_words_right_context = []
                    for index, w in enumerate(ls_words_uncertain[i + 1:]):
                        if w == '*':
                            if index < len(ls_words_uncertain) - 1:
                                remain_context = ' '.join(ls_words_uncertain[index + i + 1:])
                            break
                        ls_words_right_context.append(w)
                    right_context = ' '.join(ls_words_right_context)

                candidates = []
                for wo in w_list:
                    candidate = left_context + ' ' + wo + ' ' + right_context
                    score = self.wlm.score(candidate)
                    candidates.append({'candidate': candidate, 'word': wo, 'score': score})
                candidates = sorted(candidates, key=lambda i: i['score'], reverse=True)
                rs = rs + '; ' + error_word + ' ' + str(i) + ' ' + ' '.join([i['word'] for i in candidates[:5]])
                best_candidate = candidates[0]['candidate'] + ' ' + remain_context
                return beam_lm_(best_candidate, rs)
        return beam_lm_(text_copy, rs)
wlm_path = "/home/minh/projects/aivivn-tone/lm/corpus-wplm-4g-v2.binary"
predict = Predict(wlm_path)

if __name__ == '__main__':
    rs = predict.beam_lm('Đoái thương xom mước Việt')
    print(rs)

if 'tv' in dict:
    print('hhhhh')
