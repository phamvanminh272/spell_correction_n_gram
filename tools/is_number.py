def is_number(s):
    count = 0
    try:
        float(s)
        return True
    except ValueError:
        count += 1
    try:
        float(s.replace(',', '.'))
        return True
    except ValueError:
        count += 1
    if count == 2:
        return False
    return False
