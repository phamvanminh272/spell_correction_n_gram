import requests
import json
from return_top_5 import predict
# img_path = '/home/minh/Pictures/s1-1547998638-width720height720.jpg'
# img_path = '/home/minh/Pictures/Anh-Che-Troll-hai.jpg'
# img_path = 'http://cdn.baogiaothong.vn/upload/images/2019-1/article_img/2019-01-20/s1-1547998638-width720height720.jpg'
img_link = 'https://hinhanhdepvai.com/wp-content/uploads/2017/05/Anh-Che-Troll-hai.jpg'
# url = 'http://192.168.20.166:8080/text_recognize'
url = 'http://172.26.6.30:8000/adtechhcm/text_recognize_image'
def recognize_post(api, img_path):
    headers = {
        'Accept': '*/*',
        'Accept-Encoding': 'gzip, deflate',
        'Cache-Control': 'no-cache',
        'Connection': 'keep-alive',
        'cache-control': 'no-cache',
        'Content-Type': 'image/png',
        'content-type': 'multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW',
    }

    response = requests.post(api, files={'file': (img_path, open(img_path, 'rb'), 'image/png', headers)})
    response_dict = json.loads(response.text)

    data = response_dict['data']
    print(data)
    return data

def recognize_get(api, img_link):
    headers = {
        'Accept': '*/*',
        'Accept-Encoding': 'gzip, deflate',
        'Cache-Control': 'no-cache',
        'Connection': 'keep-alive',
        'cache-control': 'no-cache',
        'Content-Type': 'image/png',
        'content-type': 'multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW',
    }
    link = api + '?url=' + img_link.replace('https', 'http')
    print(link)
    response = requests.get(link, headers=headers)
    response_dict = json.loads(response.text)
    print(response_dict)
    data = response_dict['data']
    print(' '.join(data))
    return ' '.join(data)

def text_correction_recognize(url, img_link):
    text = recognize_get(url, img_link)
    result = predict.beam_lm(text)
    print(result)

with open('./checkpoint/common-vietnamese-syllables.txt') as f:
    dict = f.read()
def check_real_word(word):
    if word in dict:
        return True
    else:
        return False

def check_have_non_word_error(sentence):
    ls_words = sentence.split()
    for i in ls_words:
        if check_real_word(i)== False:
            return True
    return False

if __name__ == '__main__':
    # recognize_get(url, 'https://hinhanhdepvai.com/wp-content/uploads/2017/05/anh-cmt-che.jpg')
    # text_correction_recognize(url, 'https://thoidai.com.vn/stores/news_dataimages/hang.tran/102019/15/19/2613_s12-5-1705550.jpg')
    import os
    ls = []
    path = "/home/minh/Downloads/images/images_cleaned2/"
    print(os.listdir(path))
    ls_pics = os.listdir(path)
    for pic in ls_pics:
        pic_path = path + pic
        print(pic_path)
        rs = recognize_post(url, pic_path)
        print(type(rs))
        rs2 = []
        for i in rs:
            if check_have_non_word_error(i.lower()):
                rs2.append(i)
                print(i)
        rs = rs2
        if len(rs)==0:
            continue
        item = {'name_pic': pic, 'result': rs}
        ls.append(item)
        ls = sorted(ls, key=lambda i: i['name_pic'], reverse=False)
        # with open('ordered_results.txt', 'a+') as f:
        #     f.write(pic + ': ' + str(rs) + '\n')
    print(ls)
    for i in ls:
        with open('ordered_results5.txt', 'a+') as f:
            f.write(str(i) + '\n')