# Giải thích
- File từ điển: ./checkpoint/new_dictionary.txt
- File 10000 câu (lấy 8240 lấy của m, với phần còn lại lấy từ file 2tr câu):
    ./data/data_test_v3/dataset_10000_v2.txt
- File tính accuracy cũ là: test_2.py
    Run file này sẽ print ra "8239 6279 1960" (tổng số câu, số câu đúng, số câu sai)
- Một số chức năng: 
    + Hàm check English, số, chữ viết tắt nằm trong ./tools/check_number_english.py (gọi hàm để sử dụng)

# Chú ý 
- Muốn chạy test_v2.py thì sửa path dẫn tới language model để chạy

# Một vài kết quả test 
Với tập test 8240 mẫu (ta chỉ quan tâm tới các từ ta đánh dấu nó là non-word error)
 - Với 4-grams: "6280 1960" (số câu đúng, số câu sai)
 - Với 3-grams: "5724 2515" (số câu đúng, số câu sai)
 - Với 2-grams: "4775 3464" (số câu đúng, số câu sai)