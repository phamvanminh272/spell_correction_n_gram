from test_2 import predict, preprocessing
# with open('./checkpoint/new_dictionary.txt') as f:
#     dict = f.read()
with open('./checkpoint/common-vietnamese-syllables.txt') as f:
    dict = f.read()
def check_real_word(word):
    if word in dict:
        return True
    else:
        return False
def check_have_non_word_error(sentence):
    ls_words = sentence.split()
    for i in ls_words:
        if check_real_word(i)== False:
            return True
    return False
# đọc file
# with open('/home/minh/Downloads/images/labels_test.txt', 'r') as f:
#     lines = f.readlines()
with open('labels_vinh2.txt', 'r') as f:
    lines = f.readlines()
# print(lines)


# chỉ lấy mẫu có lỗi sai non-word error.
data_test_non_word_error = []
non_word_error_test = []
index_correction_non_word_error = []
for line in lines:
    # print(line.split(',')[1:])
    sentences = line.split(',')
    # bỏ đi các mẫu không đủ "câu sai, câu đúng"
    if len(sentences)<2:
        continue

    original_sentence = sentences[0].strip().lower()
    label_sentence = sentences[1].strip().lower()
    # print(original_sentence)
    # print(label_sentence)
    if check_have_non_word_error(original_sentence) == True:
        item = {'original_sentence': original_sentence, 'label_sentence': label_sentence}
        data_test_non_word_error.append(item)
        non_word_error_test.append(original_sentence)
        ls_word_original = original_sentence.split()
        ls_word_label = label_sentence.split()
        value = ''
        for index, w in enumerate(ls_word_original):
            w = w.lower()
            if check_real_word(w) == False:
                value += str(index) + ' ' + w + ' ' + ls_word_label[index] + ' ,'
        index_correction_non_word_error.append(value)
# for i in data_test_non_word_error:
#     print(i)
# print(len(data_test_non_word_error))
# for i in non_word_error_test:
#     print(i)
# for i in index_correction_non_word_error:
#     print(i)


# bắt đầu tính các độ đo
count_right = 0
count_wrong = 0
for index, text in enumerate(non_word_error_test):
    if count_right + count_wrong == 200000:
        break
    # print(index, count_right, count_wrong)
    predicted_seq = preprocessing(predict.beam_lm(predicted_seq=text))
    # print(index_correction_non_word_error[index])
    print(predicted_seq)
    index_non_word_error = index_correction_non_word_error[index]
    ls_index_non_word_error = index_non_word_error.split(',')
    ls_index_non_word_error = [i for i in ls_index_non_word_error if i != '']
    print(ls_index_non_word_error)
    for each_non_word_error in ls_index_non_word_error:
        index_no = each_non_word_error.split()[0]
        correct_word = each_non_word_error.split()[2]
        # print(predicted_seq.split()[int(index_no)])
        # print(correct_word)
        if predicted_seq.split()[int(index_no)] == correct_word:
            count_right += 1
            continue
        count_wrong += 1
        print(predicted_seq.split()[int(index_no)])
        print(correct_word)

print(count_right, count_wrong)
print('Accuracy = Correction Precision (CP):', count_right/(count_right+count_wrong))
