from flask import Flask
from flask_restplus import Resource, Api
from datetime import datetime
import json
import sys
from nltk.tokenize import word_tokenize
import re
import itertools
from strsimpy.jaro_winkler import JaroWinkler
import torch
from lm.lm import KenLM
import unidecode
import enchant # check word in english
app = Flask(__name__)
api = Api(app)
with open('./checkpoint/all-vietnamese-syllables.txt') as f:
    dict = f.read()
dict_not_tone = unidecode.unidecode(dict)
dict_ls = [i for i in word_tokenize(dict)]

def check_real_word(word):
    if word in dict:
        return True
    else:
        return False

def generate_similar_words(word):
    start = datetime.now()
    jarowinkler = JaroWinkler()
    word_not_accent = unidecode.unidecode(word)
    x = []
    for c in itertools.combinations(word_not_accent, 2):
        x.append(c)
    print(x)
    s = []
    threshold = 0.6
    if len(word)>3:
        threshold=0.8
    for i in x:
        for w in dict_ls:
            w_not_tone = unidecode.unidecode(w)
            if i[0] in w_not_tone and i[1] in w_not_tone:
                if jarowinkler.similarity(word, w)>threshold:
                    s.append(w)

    print('Duration:', datetime.now() - start)
    print(s)
    return s
class Predict:
    def __init__(self, wlm_path):
        print('Loading language model ...')
        self.wlm = KenLM(wlm_path)
    def beam_lm(self, predicted_seq, k=500, threshold=0.99):
        # replace uncertain characters with placeholders
        sentence = predicted_seq.lower()
        words = word_tokenize(sentence)
        for index, word in enumerate(words):
            if not check_real_word(word):
                words[index] = '*'
        predicted_seq_copy = ' '.join(words)
        # left_contexts is a list of previous top-k scoring substrings
        def beam_lm_(predicted_seq, predicted_seq_uncertain, k=k, step=0):
            # stop condition = no disagreements left
            uncertainties = [m.span() for m in re.finditer('\\*+', predicted_seq_uncertain)]
            if len(uncertainties) == 0:
                return predicted_seq_uncertain
            # forward
            ls_words_predicted_seq = word_tokenize(predicted_seq)
            ls_words_uncertain = word_tokenize(predicted_seq_uncertain)
            topk_fwd = [predicted_seq_uncertain[0:uncertainties[0][0]]]
            for i, v in enumerate(ls_words_uncertain):
                if v != '*':
                    continue
                c = ls_words_predicted_seq[i]

                # get a list of possible characters to search
                c_list = generate_similar_words(c)
                # get contexts
                left_context = ' '.join(ls_words_uncertain[:i])
                if i==0:
                    left_context = ''
                right_context = ''
                if i < len(ls_words_uncertain)-1:
                    ls_temp = []
                    for w in ls_words_uncertain[i + 1:]:
                        if w == '*':
                            break
                        ls_temp.append(w)
                    right_context = ' '.join(ls_temp)
                else:
                    right_context = ''
                candidates = []
                scores = torch.empty(len(c_list)).fill_(-float('inf'))
                # score candidates
                j = 0
                for ch in c_list:
                    candidate = left_context + ' ' + ch + ' ' + right_context
                    score = self.score(candidate)
                    candidates.append(candidate)
                    scores[j] = score
                    j += 1
                # print(candidates)
                # get top-k candidates
                if len(candidates) > 0:
                    _, topk_fwd_scores = torch.topk(scores, k=min(k, len(candidates)))
                    current_topk_fwd = [candidates[s] for s in topk_fwd_scores.tolist()]
                    if len(current_topk_fwd) > 0:
                        topk_fwd = current_topk_fwd
                    else:
                        topk_fwd = [left_context + unidecode.unidecode(c) + right_context for left_context in left_contexts]
                else:
                    topk_fwd = [left_context + unidecode.unidecode(c) + right_context for left_context in left_contexts]
            out= topk_fwd[0]
            print(out)
            # combine and find disagreements between ltr and rtl beam search
            # if 10 recursive calls made, fall back on exhaustive search
            if step < 10:
                # out = []
                # find disagreements
                # for i in range(len(topk_fwd[0])):
                #     if topk_fwd[0][i] == topk_bwd[0][i]:
                #         out.append(topk_fwd[0][i])
                #     else:
                #         out.append('*')
                # recursive call
                return beam_lm_(predicted_seq, out, k=k, step=step+1)
            else:
                return out
                out = []
                # find disagreements
                # for each disagreement, get top-5 candidates from ltr and rtl search TODO: hard-coded magic number
                for i in range(len(topk_fwd[0])):
                    if topk_fwd[0][i] == topk_bwd[0][i]:
                        out.append(topk_fwd[0][i])
                    else:
                        topc = set()
                        for j in range(min(5, len(topk_fwd))):
                            topc.add(topk_fwd[j][i])
                        for j in range(min(5, len(topk_bwd))):
                            topc.add(topk_bwd[j][i])
                        out.append(topc)
                # cartesian product
                candidates = []
                for i in itertools.product(*out):
                    candidates.append(''.join(i))
                # find top-1
                best_score = -1000000.0
                best_candidate = ""
                for candidate in candidates:
                    score = self.score(self.match_punct(candidate, line_raw))
                    if score > best_score:
                        best_score = score
                        best_candidate = candidate

                return best_candidate

        return beam_lm_(sentence, predicted_seq_copy)
    def score(self, candidate):
        return self.wlm.score(candidate)


wlm_path = "/media/minh/DOCUMENTS/corpus/corpus-wplm-4g-v2.binary"
predict = Predict(wlm_path=wlm_path)

@api.route('/spell/<string:text>')
class Spell(Resource):
    def get(self, text):
        start = datetime.now()
        predicted_seq = predict.beam_lm(predicted_seq=text)
        duration = datetime.now() - start
        dic = {'duration': str(duration), 'result': predicted_seq}
        return dic


if __name__ == '__main__':
    # reload(sys)
    # sys.setdefaultencoding('utf-8')
    app.run(debug=True, port=8080)
